package org.acme;


import static com.kumuluz.ee.common.attributes.ClasspathAttributes.*;
import static com.kumuluz.ee.common.dependencies.EeComponentType.*;
import static com.kumuluz.ee.jetty.JettyAttributes.*;

import java.lang.reflect.Field;

import org.eclipse.jetty.webapp.WebAppContext;

import com.kumuluz.ee.common.Extension;
import com.kumuluz.ee.common.config.EeConfig;
import com.kumuluz.ee.common.dependencies.EeComponentDependency;
import com.kumuluz.ee.common.dependencies.EeExtensionDef;
import com.kumuluz.ee.common.wrapper.KumuluzServerWrapper;

import lombok.SneakyThrows;

@EeExtensionDef(name = "Test", group = "test")
@EeComponentDependency(JAX_RS)

public class KPatch implements Extension {
	
	//	hooks into Kumuluzz to append test classes to scan path. 
	// see https://github.com/kumuluz/kumuluzee/issues/132.
	
	@Override @SneakyThrows
	public void init(KumuluzServerWrapper server, EeConfig eeConfig) {
		
		Field field = server.getServer().getClass().getDeclaredField("appContext");
		field.setAccessible(true);
		WebAppContext ctx = (WebAppContext) field.get(server.getServer());
		ctx.setAttribute(jarPattern, exploded_test+"|"+ctx.getAttribute(jarPattern));
		
		
	}

	@Override
	public void load() {
		
	}
}
