package org.acme;
import static javax.ws.rs.client.ClientBuilder.*;
import static javax.ws.rs.core.Response.Status.Family.*;
import static org.junit.Assert.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kumuluz.ee.EeApplication;

public class MyCdiTest {
	
	@BeforeClass
	public static void startserver() {
		
		
		new EeApplication();
	}
	
	
	@Test
	public void can_call_myapi() {
		
		Client client = newBuilder().build();

		Response response = client.target("http://localhost:8080/myapi").request().get();
		
		assertEquals(SUCCESSFUL, response.getStatusInfo().getFamily());
		
	}
	
	
	@Test
	public void illegal_state_exception_map_to_400() {
		
		Client client = newBuilder().build();

		Response response = client.target("http://localhost:8080/mytestapi/illegal").request().get();
		
		Assert.assertEquals(Status.BAD_REQUEST, response.getStatusInfo().toEnum());
		
	}

}
