package org.acme;
import static javax.ws.rs.client.ClientBuilder.*;
import static javax.ws.rs.core.Response.Status.Family.*;
import static org.junit.Assert.*;

import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kumuluz.ee.EeApplication;

public class MyTest {
	
	@BeforeClass
	public static void startserver() {
		
		
		new EeApplication();
		
	}
	
	@Before @SuppressWarnings("all")
	public void selfinject() {
		
		//self-inject
		
		BeanManager bm =CDI.current().getBeanManager();
		bm.createInjectionTarget((AnnotatedType) bm.createAnnotatedType(this.getClass())).inject(this, bm.createCreationalContext(null));
	}
	
	
	@Test
	public void can_call_myapi() {
		
		Client client = newBuilder().build();

		Response response = client.target("http://localhost:8080/myapi").request().get();
		
		assertEquals(SUCCESSFUL, response.getStatusInfo().getFamily());
		
	}
	
	
	@Test
	public void illegal_state_exception_map_to_400() {
		
		Client client = newBuilder().build();

		Response response = client.target("http://localhost:8080/mytestapi/illegal").request().get();
		
		assertEquals(Status.BAD_REQUEST, response.getStatusInfo().toEnum());
		
	}
	
	@Inject
	MyBean bean;
	

	@Test
	public void injection_works() {
		
		assertNotNull(bean);
		
	}
	
	
	@Inject
	MyTestBean testbean;
	

	@Test
	public void injection_works_with_test_classes() {
		
		assertNotNull(testbean);
		
	}

}
