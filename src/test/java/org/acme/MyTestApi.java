package org.acme;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("mytestapi")
public class MyTestApi {

	
	
	@Path("illegal")
	@GET
	public String illegal_state_exceptions_return_404() {
		
		throw new IllegalStateException("trouble");
	}

}
