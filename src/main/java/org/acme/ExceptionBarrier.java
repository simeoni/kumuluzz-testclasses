package org.acme;

import static javax.ws.rs.core.Response.*;
import static javax.ws.rs.core.Response.Status.*;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

public class ExceptionBarrier {

	@Provider
	public static class IllegalStateMapper implements ExceptionMapper<IllegalStateException> {

		
		@Override
		public Response toResponse(IllegalStateException e) {
			
			return status(BAD_REQUEST).build();
		
		}

	}
}
