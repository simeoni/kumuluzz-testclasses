package org.acme;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("myapi")
public class MyApi {


	@GET
	public String salute() {
		
		return "hello world";
	}
}
